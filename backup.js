module.exports = class DiscordBackup {
	constructor(guild) {
		this.guild = guild;
		this.data = {};
	}

	async backupGuild() {
		// includes emojis, stickers, roles
		this.data.guild = await this.guild.client.api.guilds(this.guild.id).get();
	}

	async backupMembers() {
		this.data.members = await this.guild.client.api.guilds(this.guild.id).members.get({query: {limit: 1000}});
	}

	async backupChannel(channelId) {
		var data = {
			channel: await this.guild.client.api.channels[channelId].get(),
			messages: []
		};
		this.data.channels ||= [];
		this.data.channels.push(data);
		do {
			var messages = await this.guild.client.api.channels[channelId].messages.get({query: {
				before: messages?.at(-1)?.id, limit: 100
			}});
			data.messages.push(...messages);
		} while (messages.length > 0);
	}

	async backupEverything() {
		await this.backupGuild();
		await this.backupMembers();
		for (let channelId of this.guild.channels.cache.keys()) {
			try {
				await this.backupChannel(channelId);
			} catch (error) {
				console.error(error.stack);
			}
		}
	}

	serialize() {
		return JSON.stringify(this.data);
	}

}