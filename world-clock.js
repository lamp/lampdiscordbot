var client = require("./client");
var config = require("./config");

client.once("ready", () => {
	(function clock() {
		var d = new Date();
		for (let x of config.world_clock) {
			let t = x.flag + ' ' + Intl.DateTimeFormat("en", {
				timeZone: x.timezone,
				hour: 'numeric',
				//minute: 'numeric',
				timeZoneName: 'long',
				hour12: true
			}).format(d);
			t = t.replace(/[a-z]+(?: |$)/g, ''); // most of the abbrv are GMT+n >:(
			client.channels.resolve(x.channel)?.setName(t);
		}
		d.setMinutes(60);
		setTimeout(clock, d - Date.now());
	})();
});
