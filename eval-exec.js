var client = require("./client");
var config = require("./config");
var Discord = require("discord.js");


client.on("messageCreate", async function (message) {
	if (message.author.id == client.user.id) return;
	if (!message.member?.roles.cache.has(config.admin_role)) return;
	if (message.content.startsWith("!>")) {
		with (message) {
			try {
				var x = await eval(message.content.substring(2).trim());
			} catch(e) {
				var x = e.message;
			}
		}
		if (typeof x == "undefined") return void await message.react(config.eval_undefined_emoji);
		let t = typeof x == 'string' ? 'txt' : 'js';
		if (typeof x != 'string' && typeof x != "function") x = require('util').inspect(x, {depth: 1});
		let cb = `\`\`\`${t}\n${x}\`\`\``;
		if (cb.length <= 2000)
			message.channel.send(cb);
		else
			message.channel.send({files:[{
				attachment: Buffer.from(x),
				name: `output.${t}`
			}]});
	}
	else if (message.content.startsWith("!$")) {
		let cp = require("child_process").spawn("bash", ["-c", message.content.substr(2).trim()]);
		function ondat(a) {
			try {
				var split = Discord.Util.splitMessage(a.toString(), {split:{char:'\n',length:2000}});
			} catch(x) {
				var split = Discord.Util.splitMessage(a.toString(), {split:{char:'',length:2000}});
			}
			split.forEach(message.channel.send.bind(message.channel));
		}
		cp.stdout.on("data", ondat);
		cp.stderr.on("data", ondat);
	}
});
