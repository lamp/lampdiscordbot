var {login} = require("masto");
var config = require("./config");
var client = require("./client");
var {WebhookClient} = require("discord.js");

var webhook = new WebhookClient({url: config.masto_webhook});
module.exports.webhook = webhook;

client.once("ready", async () => {
	var donger = await login(config.masto);
	console.log("donger logged in");
	module.exports.donger = donger;
	(async function openStream() {
		try {
			var stream = await donger.stream.streamUser();
			module.exports.stream = stream;
			stream.on("update", toot => {
				
				if (toot.account.id != config.masto_account_id) return;
				if (toot.visibility != "public") return;
				if (toot.inReplyToAccountId && toot.inReplyToAccountId != toot.account.id) return;

				webhook.send(toot.url || toot.reblog.url); //todo maybe custom embed

				//todo maybe handle deletes
			});
			stream.ws.on("close", () => {
				console.log("donger stream closed");
				setTimeout(openStream, 10000);
			});
		} catch (error) {
			console.error("donger stream", error.message);
			setTimeout(openStream, 60000);
		}
	})();
});



