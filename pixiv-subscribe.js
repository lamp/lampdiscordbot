var client = require("./client");
var DataStore = require("./datastore");
var fetch = require("node-fetch");
var {embedPixiv} = require("./pixiv-embedder");
var config = require("./config");

async function check(tag, channel) {
	var ds = new DataStore(`s${tag}`);
	var t = encodeURIComponent(tag);
	var data = await (await fetch(`https://www.pixiv.net/ajax/search/artworks/${t}?word=${t}&order=date_d&mode=all&p=1&s_mode=s_tag_full&type=all&lang=en`)).json();
	var illusts = data.body.illustManga.data;
	var newPosts = [];
	for (let owo of illusts) {
		if (!ds.get(owo.id)) {
			newPosts.push(owo);
			ds.put(owo.id);
		} else break;
	}
	for (let i = newPosts.length - 1; i >= 0; i--) {
		let url = `https://www.pixiv.net/en/artworks/${newPosts[i].id}`;
		await embedPixiv(
			client.channels.resolve(channel),
			[url],
			undefined,
			true
		);
	}
}
module.exports.check = check;
module.exports.interval = setInterval(function() {
	config.pixiv_subscriptions.forEach(x => check(x.tag, x.channel));
}, 1000*60*60);
