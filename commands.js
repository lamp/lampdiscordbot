var client = require("./client");
var config = require("./config");
var Discord = require("discord.js");
var BaseCommandInteraction = require("discord.js/src/structures/BaseCommandInteraction.js")

var commands = module.exports = [
	{
		name: "say",
		description: "test command",
		options: [
			{
				name: "text",
				description: "text to say",
				type: 3, // string
				required: true
			}
		],
		exec: i => {
			i.reply(i.options.getString("text") || "bruh");
		}
	},
	{
		name: "owo",
		description: "random owo",
		options: [
			{
				name: "length",
				description: "length of owo",
				type: 4, // integer
				required: false
			}
		],
		exec: i => {
			let owo = Math.round(Math.random()) ? 'O' : 'o';
			let n = Math.min(i.options.getInteger("length") || 9, 1998);
			for (let i = 0; i < n; i++) owo += ['o','w','O','W'][Math.floor(Math.random() * 4)];
			owo += owo = Math.round(Math.random()) ? 'O' : 'o';
			i.reply(owo);
		}
	},
	{
		name: "avatar",
		description: "View a user's original avatar (and save permanently as attachment)",
		options: [
			{
				name: "user",
				description: "can i not leave some obvious descriptions blank?",
				type: "USER"
			}
		],
		exec: async i => {
			var user = i.options.getUser("user") || i.user;
			var au = user.avatarURL({size:4096, dynamic: true});
			var an = au.split('/').pop(); an = an.substring(0, an.indexOf('?'));
			i.reply({files: [{attachment: au, name: an}], embeds: [{
				title: `Avatar for ${user.tag}`,
				url: au,
				image: {url: `attachment://${an}`},
				color: (await require('./colors').getColorRoleFor(user.id)).color
			}]});
		}
	},
	{
		name: "steal",
		description: "Copy an emoji to this server",
		options: [
			{
				name: "emoji",
				description: "The emoji to steal",
				type: "STRING",
				required: true
			},
			{
				name: "name",
				description: "Optional rename of stolen emoji",
				type: "STRING",
				required: false
			}
		],
		exec: async i => {
			await i.deferReply();
			try {
				var input = i.options.getString("emoji") || (await i.channel.messages.fetch(i.targetId))?.content;
				var emoji = Discord.Util.parseEmoji(input);
				if (!emoji.id || !emoji.name) return void await i.editReply({content: `Invalid emoji input: ${input}`});
				var url = client.rest.cdn.Emoji(emoji.id, emoji.animated ? 'gif' : 'png');
				var emoji2 = await i.guild.emojis.create(url, i.options.getString("name") || emoji.name);
				await i.editReply({content: emoji2.toString()});
			} catch (error) {
				await i.editReply({content: error.message});
			}
		}
	},
	{
		name: "Steal Emoji",
		type: "MESSAGE",
		exec: i => commands.find(x => x.name == "steal").exec(i)
	},
	{
		name: "setserverbanner",
		description: "Set the server banner image",
		options: [
			{
				name: "url",
				description: "HTTP(S) URL to an image",
				type: "STRING"
			}
		],
		exec: async i => {
			var url = i.options.getString("url");
			try {
				if (!url) {
					await i.guild.setBanner(null);
					await i.reply("cleared the server banner");
				} else {
					if (/^https?:\/\//i.test(url)) {
						await i.guild.setBanner(url);
						await i.reply(url);
					} else {
						await i.reply("http image url only!");
					}
				}
			} catch (error) {
				await i.reply(error.message);
			}
		}
	},
	{
		name: "setservericon",
		description: "Change the server icon",
		options: [
			{
				name: "url",
				description: "HTTP(S) URL to an image",
				type: "STRING"
			}
		],
		exec: async i => {
			var url = i.options.getString("url");
			try {
				if (!url) {
					await i.guild.setIcon(null);
					await i.reply("cleared the server icon");
				} else {
					if (/^https?:\/\//i.test(url)) {
						await i.guild.setIcon(url);
						await i.reply(url);
					} else {
						await i.reply("http image url only!");
					}
				}
			} catch (error) {
				await i.reply(error.message);
			}
		}
	},
	{
		name: "getemoji",
		description: "Generate a URL for an emoji",
		options: [
			{
				name: "emoji",
				description: "The emoji (code) or the name of the emoji (case-sensitive)",
				type: "STRING",
				required: true
			},
			{
				name: "format",
				description: "choose image format",
				type: "STRING",
				choices: [
					{name: "PNG", value: "png"},
					{name: "JPG", value: "jpg"},
					{name: "WEBP", value: "webp"},
					{name: "GIF", value: "gif"}
				]
			},
			{
				name: "size",
				description: "choose image size",
				type: "INTEGER",
				choices: "16,20,22,24,28,32,40,44,48,56,60,64,80,96,100,128".split(',').map(s => ({name: s, value: Number(s)}))
			}
		],
		exec: i => {
			var emojiname = i.options.getString("emoji");
			if (emojiname.startsWith('<') && emojiname.endsWith('>')) emoji = Discord.Util.parseEmoji(emojiname);
			else {
				if (emojiname.startsWith(':')) emojiname = emojiname.slice(1);
				if (emojiname.endsWith(':')) emojiname = emojiname.slice(-1);
				var emoji = client.emojis.cache.find(e => e.name == emojiname);
				if (!emoji) emoji = client.emojis.cache.find(e => e.name.toLowerCase() == emojiname.toLowerCase());
				if (!emoji) return void i.reply(`could not find emoji named ${emojiname}`);
			}
			if (!emoji.id) return void i.reply(`invalid input`);
			var qs = [];
			var size = i.options.getInteger("size");
			if (size) qs.push(`size=${size}`);
			var format = i.options.getString("format");
			if (!format) format = emoji.animated ? "gif" : "png";
			if (format == "gif" && !emoji.animated) return void i.reply(`Non-animated emoji is not available as GIF.`);
			if (format == "webp") qs.push(`quality=lossless`);
			var url = `https://media.discordapp.net/emojis/${emoji.id}.${format}`;
			if (qs.length > 0) url += '?' + qs.join('&');
			i.reply(`${emoji.name}.${format}\n${url}`);
		}
	}
];

client.on("interactionCreate", interaction => {
	if (interaction instanceof BaseCommandInteraction)
		commands.find(x => x.name == interaction.commandName)?.exec?.(interaction);
});
client.once("ready", async () => {
	let global_commands = commands.filter(x => x.global);
	let guild_commands = commands.filter(x => !x.global);
	let guild = client.guilds.resolve(config.guild);
	await guild.commands.set(guild_commands);
	await client.application.commands.set(global_commands);
});