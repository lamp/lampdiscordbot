var express = require("express");
var crypto = require("crypto");
var child_process = require("child_process");
var config = require("./config");
var client = require("./client");

var app = module.exports = express();

app.get("/update", (req, res) => {
	if (!req.query.payload) return res.status(400).send("missing payload");
	if (!req.headers["x-gitea-signature"]) return res.status(400).send("missing signature");
	if (crypto.createHmac("sha256", config.secret).update(req.query.payload).digest("hex") != req.headers["x-gitea-signature"])
		return res.status(400).send("Signature verification failed");
	let data = JSON.parse(req.query.payload);
	res.sendStatus(200);
	if (data.commits.some(commit => commit.message.includes("%%"))) return;
	child_process.exec("git fetch && git reset --hard origin/master", function(error) {
		if (error) return void client.channels.resolve(config.bot_channel)?.send(`git pull: ${error.message}`);
		child_process.exec("npm install", async function(error) {
			if (error) return void client.channels.resolve(config.bot_channel)?.send(`npm install: ${error.message}`);
			await client.channels.resolve(config.bot_channel)?.send('t');
			process.exit();
		});
	});
});

app.get("/", (req, res) => {
	res.type("text/plain").send(require("util").inspect(client));
});


app.server = app.listen(9251);