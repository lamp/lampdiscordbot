var commands = require("./commands");
var app = require("./www");
var config = require("./config");

var mediaProxyUAs = [
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 11.6; rv:92.0) Gecko/20100101 Firefox/92.0",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:38.0) Gecko/20100101 Firefox/38.0"
];
var img = Buffer.from("iVBORw0KGgoAAAANSUhEUgAAACMAAAAhCAYAAABTERJSAAACpUlEQVRYCa2XvW7bQAzH6SRApyJAkSHwlqHoJHSNgSx+lQx9iD5AH6KDX0VLAHctNHboFmQoCnTs0Kb40yJL8cg7WY4A43Q8fvxM8k7Sik64rq/ePtfMn358W9XW/dqFF7TmFuBxuKPV9S40eX66p3VHCjsHbDa5QDx+viTadCFAKtwPtP7wi5drULNgALIIwtONUBlQE+bFQASsAlSFURAi2n78K+507D+dHV8yWCdAKYyCbDrabr8qQHSzCCoAOoucWxC7jqB9//7wQ1bGK8qarKXjpiP0IWKJTpgZhhnuRCcf94OWD5BLrnX3QNLQBUyWFdTZZmBRaSJaU6740AvOEQsCn5j3feSdwh5LM8exHtjR/8LHfo+WZs0OebYmQSYwWiJZPWHkMgb2EZA08gSGbYMSQe6d+7nEld3myxr5EBt5vJQ9sx/ig2zTpT2iTls3mQ/EJKISxjm0aU2b0NksnZZlMp4Awmk3B5xZrt76Mvp5ZBxnZjxT1AH3Uf2RUDjPSuIVxxJBHMLwGdJ6CLpDUGLwH0g2gehkY1Gm319+HnStQ0MvjqLdgjXIbZ+JfjiOfiXmJDN4Rtzs8CI1ZYwCh82cZKsGgrWb3Tk/nyYwOPTeXb2Otzb+deNVAo5DSE8TZBoqCqOnry3P6GRugCiDngMPXHsdSnTOIoapgVjD6n1rxzkI8SUlwvziZBDTJ0UGEwABsVlhGF4ISiMGdsx6Rs+jRnDrCyA2K1jTnrGK2b3+c38oZgaJPAKB6nQPJ8aFeGYmCzsiykCge1RmYJCVKgrsZTUQ6B4yc0StUSrtER8tmQMCn7e+R7w6v5Av2VF4q/9+/4de3b7xPnkuWZBF+QKQeTTq1wGAoND8pjZv82ITOYZsDoC1VRgRtgIsCSK+W+M/mSFEURCM3rAAAAAASUVORK5CYII=","base64");

commands.push({
	name: "count",
	description: "Estimate how many clients are looking at the channel",
	exec: async i => {
		var count = 0;
		var route = `/tmp/${Math.random()}`;
		app.get(route, (req, res) => {
			if (mediaProxyUAs.includes(req.headers["user-agent"])) count++;
			res.type("png").send(img);
		});
		await i.reply(config.base_uri + route);
		await new Promise(r => setTimeout(r, 1000));
		await i.editReply(`It appears there might be **${count}** clients viewing this channel right now.`);
	}
});
