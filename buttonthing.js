var client = require("./client");
var config = require("./config");
var fs = require("fs");


var data = fs.existsSync("data/buttonthing.json") ? JSON.parse(fs.readFileSync("data/buttonthing.json", "utf8")) : ({
	userVoteMap: {},
	votes: {
		miku: 0,
		teto: 0,
		both: 0,
		what: 0
	}
});

function setVote(user, vote) {	
	var existingVote = data.userVoteMap[user];
	if (vote == existingVote) {
		data.votes[existingVote]--;
		data.userVoteMap[user] = undefined;
	} else {
		if (existingVote) data.votes[existingVote]--;
		data.votes[vote]++;
		data.userVoteMap[user] = vote;
	}
	fs.writeFileSync("data/buttonthing.json", JSON.stringify(data));
}


var thing = () => ({
	content: "Miku or Teto?",
	components: [
		{
			type: "ACTION_ROW",
			components: [
				{
					type: "BUTTON",
					label: `Miku (${data.votes.miku})`,
					customId: "miku",
					style: "SUCCESS",
					emoji: "<:MikuSquish:868670741518888961>"
				},
				{
					type: "BUTTON",
					label: `Teto (${data.votes.teto})`,
					customId: "teto",
					style: "DANGER",
					emoji: "<:tetodrill2:705605869571801120>"
				},
				{
					type: "BUTTON",
					label: `both/idk (${data.votes.both})`,
					customId: "both",
					style: "PRIMARY",
					emoji: "<:squee:707727925587345490>"
				},
				{
					type: "BUTTON",
					label: `what/idk (${data.votes.what})`,
					customId: "what",
					style: "SECONDARY",
					emoji: "😕"
				}
			]
		}
	]
});

module.exports.init = function(channel = client.channels.resolve(config.announcement_channel)) {
	channel.send(thing());
}

client.on("interactionCreate", i => {
	if (!Object.keys(data.votes).includes(i.customId)) return;
	setVote(i.user.id, i.customId);
	i.update(thing());
});