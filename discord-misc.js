var client = require("./client.js");
var config = require("./config");


client.on("guildMemberAdd", member => {
	if (member.guild.id != config.guild) return;
	// add role
	member.roles.add(member.user.bot ? config.bot_role : config.human_role);
	// welcome message
	/*setTimeout(() => {
		client.channels.resolve(config.default_channel)?.send(
			`Welcome ${member}. Please tell from where you entered this server and some other info about yourself in order to gain access to message history.`
		);
	}, 3000);*/
	member.roles.add(config.verified_role);
});
// join message
/*client.on("messageDelete", message => {
	if (message.channel.id != config.default_channel) return;
	if (message.type != "GUILD_MEMBER_JOIN") return;
	client.channels.resolve(config.default_channel)?.send(
		`sussy baka ${message.author} deleted their join message ||(from <t:${Math.floor(message.createdAt.valueOf()/1000)}>)||`
	);
});*/
client.on("guildMemberRemove", member => {
	if (member.guild.id != config.guild) return;
	// leave message
	client.channels.resolve(config.default_channel)?.send(random([
		`${member.user.username} left`,
		`${member.user.username} disappeared`,
		`${member.user.username.toLowerCase()} is gone`
	]));
});


client.on("messageCreate", message => {
	// comment thread on announcements
	message.channel.id == config.announcement_channel && message.startThread({name: "Comments"});
	// stupid m bot
	message.author.id == "732072478519722096" && message.content.endsWith("is bad letter m is much better") && message.delete();
});


// add reactions to video and audio
{
	let a = async m => {
		if (m.guild?.id != config.guild) return;
		if ((m.embeds.some(e => e.type == "video") || m.attachments.some(a => a.contentType?.startsWith('video'))) && !m.g) {
			m.g = true;
			m.react(config.mi_emoji);
		}
		if (m.attachments.some(a => a.contentType?.startsWith('audio')) && !m.d) {
			m.d = true;
			m.react(config.ki_emoji);
		}
	}
	client.on("messageCreate", a);
	client.on("messageUpdate", (r, q) => a(q));
}

// thing to access archived channels
client.on("voiceStateUpdate", (oldState, newState) => {
	if (newState.guild.id != config.guild) return;
	if (oldState.channelId != config.archive_portal_voice_channel && newState.channelId == config.archive_portal_voice_channel) {
		// join
		newState.member?.roles.add(config.view_archived_channels_role);
	} else if (oldState.channelId == config.archive_portal_voice_channel && newState.channelId != config.archive_portal_voice_channel) {
		// leave
		newState.member?.roles.remove(config.view_archived_channels_role);
	}
});

// save deleted emojis
client.on("emojiDelete", emoji => {
	client.channels.resolve(config.bot_channel)?.send({
		content: "emoji deleted",
		files: [{attachment: emoji.url, name: `${emoji.name}.${emoji.url.split('.').pop()}`}]
	});
});

//g=setInterval(() => client.guilds.resolve(config.guild)?.setIcon(`mf/${Math.floor(Math.random()*14548)}.png`), 1800000);