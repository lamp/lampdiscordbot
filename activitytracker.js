var client = require("./client");
var config = require("./config");
var app = require("./www");
var DataStore = require("./datastore");

var ds = new DataStore("activity");


app.get("/detect/:code", (req, res) => {
	res.sendFile(process.cwd() + "/track-image.png");
	if (req.headers["user-agent"].includes("Discordbot")) return;
	onActivity(ds.get(req.params.code));
	ds.del(req.params.code);
});


client.on("messageCreate", async m => m.guildId == config.guild && onActivity(m.author.id));
client.on("interactionCreate", async i => i.guildId == config.guild && onActivity(i.user.id));
client.on("typingStart", async t => t.guild?.id == config.guild && onActivity(t.user.id));
client.on("guildMemberAdd", async m => m.guild.id == config.guild && onActivity(m.user.id));
client.on("messageReactionAdd", async (r, user) => r.message?.guild?.id == config.guild && onActivity(user.id));
client.on("messageReactionRemove", async (r, user) => r.message?.guild?.id == config.guild && onActivity(user.id));
client.on("messageUpdate", async (oldMessage, newMessage) => newMessage.guild?.id == config.guild && (oldMessage.editedAt != newMessage.editedAt) && onActivity(newMessage.author.id));


async function onActivity(user_id) {
	if (!user_id) return;
	let user = client.users.resolve(user_id);
	if (!user || user.bot) return;
	ds.put(user_id, Date.now());
	if (ds.get(user_id + "deactivated")) {
		let member = client.guilds.resolve(config.guild).members.resolve(user_id);
		await reactivateMember(member);
		/*await client.channels.resolve(config.default_channel)?.send(random([
			`${member.displayName} bacc`,
			`${member.displayName} is bacc`,
			`hi ${member.displayName}`
		]));*/
	}
}


module.exports.interval = setInterval(async () => {
	var guild = client.guilds.resolve(config.guild);
	if (!guild) return;
	guild.members.cache.filter(m => !m.user.bot).forEach(member => {
		let lastActivityTime = ds.get(member.user.id);
		if (!lastActivityTime) return;
		if (Date.now() - lastActivityTime > 1000*60*60*72) { // if last activity > 72 hours ago
			if (!ds.get(member.id + "deactivated"))
				deactivateMember(member);
		}
	});
}, 60*60*1000); // every hour i guess


async function deactivateMember(member) {
	await member.roles.add(config.inactive_role);
	ds.put(member.id + "deactivated");
	ds.put(member.id, Date.now());

	var magic_channel = client.channels.resolve(ds.get(member.id + "magicchannelid"));
	if (!magic_channel) {
		magic_channel = await member.guild.channels.create("inactive", {
			permissionOverwrites: [
				{
					id: member.guild.roles.everyone,
					deny: "VIEW_CHANNEL"
				},
				{
					id: client.user,
					allow: "VIEW_CHANNEL"
				},
				{
					id: member,
					allow: "VIEW_CHANNEL"
				}
			]
		});
		ds.put(member.id + "magicchannelid", magic_channel.id);
	} else {
		magic_channel.permissionOverwrites.edit(member, {"VIEW_CHANNEL": true});
	}

	var magic_channel_message_id = ds.get(member.id + "magicchannelmessage");
	var unique_code = Math.random().toString();
	ds.put(unique_code, member.id);
	var content = `${config.base_uri}/detect/${unique_code}`;
	if (!magic_channel_message_id) {
		var magic_channel_message = await magic_channel.send({content});
		ds.put(member.id + "magicchannelmessage", magic_channel_message.id)
	} else {
		await magic_channel.messages.edit(magic_channel_message_id, {content});
	}

}

async function reactivateMember(member) {
	await member.roles.remove(config.inactive_role);
	ds.del(member.id + "deactivated");
	var magic_channel = client.channels.resolve(ds.get(member.id + "magicchannelid"));
	if (magic_channel) await magic_channel.permissionOverwrites.edit(member, {"VIEW_CHANNEL": false});
}

module.exports.deactivateMember = deactivateMember;
module.exports.reactivateMember = reactivateMember;


client.on("messageCreate", async message => {
	if (message.guildId != config.guild) return;
	let deactivatedMembersMentionedViaRoles = [...new Set(message.mentions.roles.flatMap(r => r.members).values())].filter(x => ds.get(x.id + "deactivated"));
	if (deactivatedMembersMentionedViaRoles.length) {
		for (let m of deactivatedMembersMentionedViaRoles) await reactivateMember(m);
		await message.reply({content: deactivatedMembersMentionedViaRoles.map(String).join(' '), allowedMentions:{repliedUser: false}});
	}
});