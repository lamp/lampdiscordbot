var fs = require("fs");
var fetch = require("node-fetch");
var Discord = require("discord.js");

var client = require("./client");
var config = require("./config");
var app = require("./www");
var commands = require("./commands");


/*commands.push({
	type: "MESSAGE",
	name: "Archive",
	exec: async i => {
		let message = await i.channel.messages.fetch(i.targetId);
		let asdf = await doThing(message, i.user);
		i.reply({
			content: `https://discord.com/channels/${asdf.guild.id}/${asdf.channel.id}/${asdf.id}`,
			ephemeral: true
		});
	}
});*/


client.on("messageReactionAdd", async (reaction, user) => {
	if (reaction.emoji.name == '📍' || reaction.emoji.name == '📌') {
		if (!reaction.message.guild) return;
		if (reaction.message.channel.id == config.archive_channel) return;
		if (reaction.message['has been pinned'] || reaction.count > 1) return;
		reaction.message['has been pinned'] = true;

		/*if (reaction.message.channel.id == config.porn_channel)*/ {
			try {
				await reaction.message.pin();
			} catch (e) {
				await message.react('⚠');
			}
			return;
		}

		if (reaction.partial) {
			try {
				await reaction.fetch();
			} catch (error) {
				console.error("reaction fetch", error.message);
				return void await message.react('⚠');
			}
		}

		doThing(reaction.message, user);
	}
});


async function doThing(message, user) {
	var avatarURL = message.author.avatarURL({dynamic: true}) || message.author.defaultAvatarURL;
	var avatarName = avatarURL.split('/').pop();
	let imageCandidate = message.attachments.find(a => [".png",".jpg",".jpeg",".webp",".gif"].some(e => a.url.toLowerCase().endsWith(e)));
	if (imageCandidate) imageCandidate["will be used for the image of the embed"] = true;
	else imageCandidate = message.embeds.find(e => e.type == 'image');
	let embed = new Discord.MessageEmbed()
		.setAuthor(message.member?.displayName || message.author.username, `attachment://${avatarName}`)
		.setDescription(message.content)
		.setImage(imageCandidate?.url)
		.setFooter(`Pinned by ${message.guild.members.resolve(user)?.displayName || user.username}`)
		.setTimestamp(message.createdAt)
		.setColor(message.member?.roles.color?.color);
	let attachments = message.attachments.filter(a => !a["will be used for the image of the embed"]).map(a => `[${a.name}](${a.url})`).join('\n');
	if (attachments) embed.addField("Attachments", attachments);
	return (await client.channels.fetch(config.archive_channel))?.send({
		content: `https://discord.com/channels/${message.guild.id}/${message.channel.id}/${message.id}`,
		files: [{attachment: avatarURL, name: avatarName}],
		embeds:[embed]
	});
}



app.use("/avatars/", require("express").static(config.data_dir + "avatars/"));
