var fetch = require("node-fetch");
var {getAverageColor} = require("fast-average-color-node");
var fs = require("fs");
var Discord = require("discord.js");

var client = require("./client");
var config = require("./config");
var commands = require("./commands");


function getColorRoleFor(user_id) {
	try {
		return client.guilds.resolve(config.guild)?.roles.fetch(
			fs.readFileSync(config.data_dir + "color/" + user_id, "utf8")
		);
	} catch (e) {}
}
module.exports.getColorRoleFor = getColorRoleFor;


client.on("guildMemberAdd", async function (member) {
	if (member.guild.id != config.guild) return;
	var role = await member.guild.roles.create({
		name: member.user.username,
		color: await user2color(member.user) || "#FF0000",
		mentionable: true,
		permissions: 0n
	});
	member.roles.add(role);
	fs.writeFileSync(config.data_dir + "color/" + member.id, role.id);
});
client.on("userUpdate", async function (oldUser, user) {
	var colorRole = await getColorRoleFor(user.id);
	if (!colorRole) return;
	if (oldUser.username != user.username)
		if (!fs.existsSync(`${config.data_dir}color/${user.id}_custom-name`))
			await colorRole.setName(user.username);
	if (oldUser.avatar != user.avatar) {
		if (fs.existsSync(`${config.data_dir}color/${user.id}_custom`)) return;
		let c = await user2color(user);
		await colorRole.setColor(c);
	}
});
client.on("guildMemberRemove", async function (member) {
	if (member.guild.id != config.guild) return;
	var colorRole = await getColorRoleFor(member.user.id);
	if (!colorRole) return;
	if (!colorRole.members.size) colorRole.delete();
});



commands.push({
	name: "setcolor",
	description: "Set yourself to a custom color",
	options: [
		{
			name: "color",
			description: "Hex string or color name.",
			type: "STRING",
			required: true
		}
	],
	exec: async i => {
		var colorRole = await getColorRoleFor(i.user.id);
		if (!colorRole) return void i.reply({content: "You don't have color role!"});
		let x = i.options.getString("color");
		if (x) {
//			if (/^[a-zA-Z_]+$/.test(x))
				x = x.toUpperCase();
//			else if (!/^#?[a-fA-F0-9]{6}$/.test(x)) {
//				let m = x.match(/(\d+)\s*,\s*(\d+)\s*,\s*(\d+)/);
//				if (m) x = [Number(x[1]), Number(x[2]), Number(x[3])];
//			}
		}
		try {
			await colorRole.setColor(x);
		} catch(error) {
			return void await i.reply({content: error.message});
		}
		fs.writeFileSync(`${config.data_dir}color/${i.user.id}_custom`, '');
		await i.reply({content: colorRole.toString()});
	}
});

commands.push({
	name: "settitle",
	description: "Change your color role name",
	options: [
		{
			name: "name",
			description: "Name of role",
			type: "STRING",
			required: true
		}
	],
	exec: async i => {
		var colorRole = await getColorRoleFor(i.user.id);
		if (!colorRole) return void i.reply({content: "You don't have color role!"});
		try {
			await colorRole.setName(i.options.getString("name"));
		} catch(error) {
			await i.reply({content: error.message});
			return;
		}
		fs.writeFileSync(`${config.data_dir}color/${i.user.id}_custom-name`, '');
		await i.reply({content: colorRole.toString()});
	}
});

commands.push({
	name: "seticon",
	description: "Set your role icon",
	options: [
		{
			name: "icon",
			description: "Image URL or an emoji",
			type: "STRING"
		}
	],
	exec: async i => {
		await i.deferReply();
		var colorRole = await getColorRoleFor(i.user.id);
		if (!colorRole) return void i.editReply({content: "You don't have color role!"});
		try {
			let icon = i.options.getString("icon");
			if (/^https?:\/\//i.test(icon)) {
				await colorRole.setIcon(icon);
				await i.editReply({files: [{
					attachment: icon,
					name: icon.match(/\/(\w*(?:\.png|\.jpg|\.jpeg|\.gif|\.webp))$/i)?.[1] || "icon.png"
				}]});
			} else if (icon) {
				var emoji = Discord.Util.parseEmoji(icon);
				if (emoji?.id) {
					await colorRole.setIcon(i.client.rest.cdn.Emoji(emoji.id, emoji.animated ? 'gif' : 'png'));
				} else {
					await colorRole.setUnicodeEmoji(icon);
				}
				await i.editReply({content: icon});
			} else {
				await colorRole.setIcon(null);
				await i.editReply({content: "icon removed"});
			}
		} catch (error) {
			await i.editReply({content: error.message});
		}
	}
});






async function user2color(user) {
	var avatarURL = user.avatarURL({format:'png', size: 16}) || user.defaultAvatarURL;
	var image = await (await fetch(avatarURL)).buffer();
	return (await getAverageColor(image)).value;
}
