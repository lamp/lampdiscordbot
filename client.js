var Discord = require("discord.js");
var config = require("./config");

var client = module.exports = new Discord.Client({
	partials: ['MESSAGE','REACTION'],
	intents: 32767 //all
});

client.login(config.token).then(async () => {
	console.log("ready");
	(await client.channels.fetch(config.bot_channel))?.send('a');
});
