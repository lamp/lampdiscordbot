module.exports = {
	token: process.env.TOKEN,
	secret: process.env.SECRET,
	deepl_auth_key: process.env.DEEPL_KEY,
	application_id: "707451582454824980",
	guild: "672956423545815040",
	admin_role: "776899554603565116",
	human_role: "672956630962274306",
	bot_role: "673671040010027034",
	inactive_role: "892869309603389500",
	verified_role: "949064806030254130",
	view_archived_channels_role: "916056534402863125",
	eval_undefined_emoji: "🅱️",
	mi_emoji: "887931046086185060",
	ki_emoji: "887935846710394910",
	default_channel: "949831184957980722",
	bot_channel: "949831221981097984",
	archive_channel: "802280618636869663",
	moe_channel: "871864787213111366",
	porn_channel: "949831237927862333",
	announcement_channel: "876010629490683955",
	miku_channel: "900583427483516938",
	archive_category: "887838689533771776",
	archive_portal_voice_channel: "916057450313023508",
	data_dir: process.cwd() + "/data/",
	base_uri: "https://ldb.owo69.me",
	world_clock: [
		{
			flag: "🇺🇸",
			timezone: "America/Los_Angeles",
			channel: "887897732428226660"
		},
		{
			flag: "🇺🇸",
			timezone: "America/New_York",
			channel: "888507872932143155"
		},
		{
			flag: "🇩🇪",
			timezone: "Europe/Berlin",
			channel: "887897886879281203"
		},
		{
			flag: "🇷🇺",
			timezone: "Europe/Moscow",
			channel: "887897904738599002"
		},
		{
			flag: "🇵🇭",
			timezone: "Asia/Manila",
			channel: "888505937315389451"
		},
		{
			flag: "🇯🇵",
			timezone: "Asia/Tokyo",
			channel: "887897753198419999"
		}
	],
	pixiv_subscriptions: [
		{
			tag: "初音ミク",
			channel: "900583427483516938"
		},
		{
			tag: "鏡音リン",
			channel: "904093976615849996"
		},
		{
			tag: "重音テト",
			channel: "904093991224627270"
		},
		{
			tag: "巡音ルカ",
			channel: "916444961958928394"
		}
	],
	vrchat_status_category: "959236139913445376",
	vrchat_configuration: {
		username: process.env.VRCHAT_USERNAME,
		password: process.env.VRCHAT_PASSWORD
	},
	masto: {
		url: "https://mastodong.lol",
		accessToken: process.env.MASTO_TOKEN
	},
	masto_account_id: "108643271047165149",
	masto_webhook: process.env.DISCORD_WEBHOOK_FOR_MASTO
}