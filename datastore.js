var fs = require("fs");
var config = require("./config");

module.exports = class DataStore {
	constructor(id) {
		if (id.includes('/')) throw new Error("NO");
		this.dir = `${config.data_dir}/${id}/`;
		if (!fs.existsSync(this.dir)) fs.mkdirSync(this.dir);
	}
	get(key) {
		if (key.includes('/')) throw new Error("NO");
		try {
			return fs.readFileSync(this.dir + key, "utf8") || true;
		} catch (error) {
			if (error.code == "ENOENT") return false;
			else throw error;
		}
	}
	put(key, value) {
		if (key.includes('/')) throw new Error("NO");
		return fs.writeFileSync(this.dir + key, String(value) || '');
	}
	del(key) {
		if (key.includes('/')) throw new Error("NO");
		try {
			return fs.unlinkSync(this.dir + key);
		} catch (error) {
			if (error.code != "ENOENT") throw error;
		}
	}
}