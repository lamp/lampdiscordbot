process.title = "lamp discord bot";
process.on("unhandledRejection", error => {
	console.error("Unhandled Rejection:\n", error.stack || error);
});

require("./util"); // global variables set in here
require("./client");
require("./discord-misc");
require('./eval-exec');
require('./colors');
require('./www');
require('./pinboard');
require('./pixiv-embedder');
require('./translate2');
require('./world-clock');
require('./buttonthing');
require("./activitytracker");
require("./vocabularygame");
require("./pixiv-subscribe");
require("./count-cmd");
require("./masto");