var commands = require("./commands");
var config = require("./config");
var deepl = require("deepl");
var Kuroshiro = require("kuroshiro").default;
var KuromojiAnalyzer = require("kuroshiro-analyzer-kuromoji");
var kuroshiro = new Kuroshiro();
kuroshiro.init(new KuromojiAnalyzer());

var lang2flag = {
	BG: '🇧🇬',
	CS: '🇨🇿',
	DA: '🇩🇰',
	DE: '🇩🇪',
	EL: '🇬🇷',
	EN: '🇬🇧',
	'EN-GB': '🇬🇧',
	'EN-US': '🇺🇸',
	ES: '🇪🇸',
	ET: '🇪🇪',
	FI: '🇫🇮',
	FR: '🇫🇷',
	HU: '🇭🇺',
	IT: '🇮🇹',
	JA: '🇯🇵',
	LT: '🇱🇹',
	LV: '🇱🇻',
	NL: '🇳🇱',
	PL: '🇵🇱',
	PT: '🇵🇹',
	'PT-PT': '🇵🇹',
	'PT-BR': '🇧🇷',
	RO: '🇷🇴',
	RU: '🇷🇺',
	SK: '🇸🇰',
	SL: '🇸🇮',
	SV: '🇸🇻',
	ZH: '🇨🇳'
};


commands.push({
	name: "toen",
	description: "Translate text to English",
	global: true,
	options: [
		{
			type: "STRING",
			name: "text",
			description: "Text to translate",
			required: true
		}
	],
	exec: i => t(i, "EN")
});
commands.push({
	name: "toja",
	description: "Translate text to Japanese",
	global: true,
	options: [
		{
			type: "STRING",
			name: "text",
			description: "Text to translate",
			required: true
		}
	],
	exec: i => t(i, "JA")
});
commands.push({
	name: "tolang",
	description: "Translate text to any language",
	global: true,
	options: [
		{
			type: "STRING",
			name: "lang",
			description: "Language to translate to",
			required: true,
			choices: [
				{ name: 'Bulgarian', value: 'BG' },
				{ name: 'Czech', value: 'CS' },
				{ name: 'Danish', value: 'DA' },
				{ name: 'German', value: 'DE' },
				{ name: 'Greek', value: 'EL' },
				{ name: 'English (British)', value: 'EN-GB' },
				{ name: 'English (American)', value: 'EN-US' },
				{ name: 'Spanish', value: 'ES' },
				{ name: 'Estonian', value: 'ET' },
				{ name: 'Finnish', value: 'FI' },
				{ name: 'French', value: 'FR' },
				{ name: 'Hungarian', value: 'HU' },
				{ name: 'Italian', value: 'IT' },
				{ name: 'Japanese', value: 'JA' },
				{ name: 'Lithuanian', value: 'LT' },
				{ name: 'Latvian', value: 'LV' },
				{ name: 'Dutch', value: 'NL' },
				{ name: 'Polish', value: 'PL' },
//				{ name: 'Portuguese (all Portuguese varieties excluding Brazilian Portuguese)', value: 'PT-PT' },
				{ name: 'Portuguese (Brazilian)', value: 'PT-BR' },
				{ name: 'Romanian', value: 'RO' },
				{ name: 'Russian', value: 'RU' },
				{ name: 'Slovak', value: 'SK' },
				{ name: 'Slovenian', value: 'SL' },
				{ name: 'Swedish', value: 'SV' },
				{ name: 'Chinese', value: 'ZH' }
			]
		},
		{
			type: "STRING",
			name: "text",
			description: "Text to translate",
			required: true
		}
	],
	exec: i => t(i, i.options.getString("lang"))
});

async function t(i, target_lang) {
	var text = i.options.getString("text");
	await i.deferReply();

	try {
		var translation = (await deepl({
			text,
			target_lang,
			free_api: true,
			auth_key: config.deepl_auth_key
		})).data.translations[0];
	} catch (error) {
		return void await i.editReply(error.message);
	}

	if (translation.detected_source_language == "JA") try {
		var input_romaji = await kuroshiro.convert(text, {to: "romaji", mode: "spaced"});
	} catch (error) {}
	if (target_lang == "JA") try {
		var output_romaji = await kuroshiro.convert(translation.text, {to: "romaji", mode: "spaced"});
	} catch (error) {}

	let input_flag = lang2flag[translation.detected_source_language] || `[${translation.detected_source_language}]`;
	let output_flag = lang2flag[target_lang] || `[${target_lang}]`;

	let msg = input_flag + ' ' + text;
	if (input_romaji) msg += '\n' + input_flag + ' ' + input_romaji;
	msg += '\n' + output_flag + ' ' + translation.text;
	if (output_romaji) msg += '\n' + output_flag + ' ' + output_romaji;

	await i.editReply(msg);

	try {
		var reverse_translation = (await deepl({
			text: translation.text,
			target_lang: translation.detected_source_language,
			free_api: true,
			auth_key: config.deepl_auth_key
		})).data.translations[0];
		if (normalize(text) != normalize(reverse_translation.text)){
			if (translation.detected_source_language == "JA") try {
				var reverse_romaji = await kuroshiro.convert(reverse_translation.text, {to: "romaji", mode: "spaced"});
			} catch (error) {}
			await i.editReply(`${msg}\n🔁 ${reverse_translation.text}${reverse_romaji ? `\n🔁 ${reverse_romaji}` : ''}`);
		}
	} catch(error) {}
}

function normalize(text) {
	return text.toLowerCase()//.split('').filter(x => x.match(/[a-z0-9 ]/)).join('');
}